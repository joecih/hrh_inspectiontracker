﻿
Namespace LightSwitchApplication

    Public Class InspectionHeaderDetail

        Private Sub InspectionHeader_Loaded(succeeded As Boolean)
            ' Write your code here.
            Me.SetDisplayNameFromEntity(Me.InspectionHeader)
        End Sub

        Private Sub InspectionHeader_Changed()
            ' Write your code here.
            Me.SetDisplayNameFromEntity(Me.InspectionHeader)
        End Sub

        Private Sub InspectionHeaderDetail_Saved()
            ' Write your code here.
            Me.SetDisplayNameFromEntity(Me.InspectionHeader)
        End Sub

    End Class

End Namespace