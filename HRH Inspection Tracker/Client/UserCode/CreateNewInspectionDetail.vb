﻿
Namespace LightSwitchApplication

    Public Class CreateNewInspectionDetail

        Private Sub CreateNewInspectionDetail_InitializeDataWorkspace(ByVal saveChangesTo As Global.System.Collections.Generic.List(Of Global.Microsoft.LightSwitch.IDataService))
            ' Write your code here.
            Me.InspectionDetailProperty = New InspectionDetail()
        End Sub

        Private Sub CreateNewInspectionDetail_Saved()
            ' Write your code here.
            Me.Close(False)
            Application.Current.ShowDefaultScreen(Me.InspectionDetailProperty)
        End Sub

    End Class

End Namespace