﻿
Namespace LightSwitchApplication

    Public Class NewInspection

        Private Sub InspectionHeaderListAddAndEditNew_CanExecute(ByRef result As Boolean)
            ' Write your code here.
            result = Me.InspectionHeaders.CanAddNew()
            result = Me.InspectionDetail.CanAddNew()
        End Sub

        ' Private Sub InspectionHeaderListAddAndEditNew_Execute()
        ' Write your code here.
        '   Dim newInspection As InspectionHeader = Me.InspectionHeaders.AddNew()
        '   Me.InspectionHeaders.SelectedItem = newInspection
        '   Me.OpenModalWindow("AddNewInspection")
        'End Sub

        Private Sub Cancel_Execute()
            ' Write your code here.
            Me.CloseModalWindow("AddNewInspection")
            InspectionHeaders.SelectedItem.Details.DiscardChanges()
        End Sub

        Private Sub CreateNewInspection_Execute()
            ' Write your code here.
            Dim newInspection As InspectionHeader = Me.InspectionHeaders.AddNew()
            Me.InspectionHeaders.SelectedItem = newInspection
            Me.OpenModalWindow("AddNewInspection")
        End Sub

        Private Sub GoToDetail_Execute()
            ' Write your code here.
            Me.CloseModalWindow("AddNewInspection")

            Dim newInspectionDetail As InspectionDetail = Me.InspectionDetail.AddNew()
            Me.InspectionDetail.SelectedItem = newInspectionDetail
            Me.OpenModalWindow("NewInspectionDetails")
        End Sub

        Private Sub CancelDetails_Execute()
            ' Write your code here.
            Me.CloseModalWindow("NewInspectionDetails")
            InspectionDetail.SelectedItem.Details.DiscardChanges()
            InspectionHeaders.SelectedItem.Details.DiscardChanges()
        End Sub

        Private Sub SaveInspection_Execute()
            ' Write your code here.
            Me.CloseModalWindow("NewInspectionDetails")
            Me.Save()
        End Sub


    End Class

End Namespace
