﻿
Namespace LightSwitchApplication

    Public Class Temp_NewInspection

        Private Sub Temp_NewInspection_InitializeDataWorkspace(saveChangesTo As List(Of Microsoft.LightSwitch.IDataService))
            ' Write your code here.
            Me.InspectionHeaderProperty = New InspectionHeader()
        End Sub

        Private Sub Temp_NewInspection_Saved()
            ' Write your code here.
            Me.Close(False)
            Application.Current.ShowDefaultScreen(Me.InspectionHeaderProperty)
        End Sub

    End Class

End Namespace