﻿
myapp.BrowseTestTable.TestTableItem_render = function (element, contentItem) {
    // Write code here.
    $(element).append("<p id='test'><font color='blue' font-size='10px'>NOw is the TIME!</font></p>");

    $(element).append("<div><p>If you click on the Hide Button, I will disappear.</p><button id='hide'>Hide</button><button id='show'>Show</button></div>");

    $("#hide").click(function () {
        $("#test").hide();
    });

    $("#show").click(function () {
        $("#test").show();
    });
};