﻿
myapp.Main.spacerContent_render = function (element, contentItem) {

    // Write code here.
    if ($('#menu')[0].innerHTML == "") {
        // Create Menu
        $('#menu').multilevelpushmenu({
            menu: arrMenu,
            collapsed: true,
            containersToPush: [$("div[data-role*='page']")],//$("div[data-role*='header']"), $("div[data-role*='content']")],
            menuWidth: '220px',
            menuHeight: '100%',
            overlapWidth: 38,

            /* MSLS Header Adjustment
            *  - On menu expanded, adjust the msls header as it inserts a gap
            *    with the push menu utility. ------------------------------------------------ */
            onExpandMenuEnd: function () {
                resetPushObj();
            },

            //// Reset MSLS header on menu collapse ----------------------------------------------
            onCollapseMenuEnd: function () {
                $("div[data-role*='page']").css("width", "100%");
            },
            onItemClick: function () {
                // First argument is original event object
                var event = arguments[0],
                // Second argument is menu level object containing clicked item (<div> element)
                $menuLevelHolder = arguments[1],
                // Third argument is clicked item (<li> element)
                $item = arguments[2],
                // Fourth argument is instance settings/options object
                options = arguments[3];
                var itemName = $item.find('a:first').text();
                // Collapse menu
                $('#menu').multilevelpushmenu('collapse');

                // Open Page
                if (itemName == 'MainMenu') {
                    myapp.navigateHome();
                }
                if (itemName == 'ShowGrid') {
                    myapp.showGrid();
                }
                if (itemName == 'WorkTypes') {
                    myapp.showBrowseWorkTypes();
                }

            }
        });
    } else {
        // Menu already created
        // Update Menu containers to push
        updateContainersToPush();
    };

};

$(window).resize(function () {
    $('#menu').multilevelpushmenu('redraw');
    resetPushObj();
});

function resetPushObj() {

    //console.log("PUSH MENU is Expanded? " + $("#menu").multilevelpushmenu('menuexpanded', $('.levelHolderClass')));
    var pmState = $('#menu').multilevelpushmenu('menuexpanded', $('.levelHolderClass'));

    if (pmState == true) {

        var winWidth = parseInt($(window).width());
        var pmWidth = parseInt($(".levelHolderClass").width()) - 40; // Minus overlap
        //var pmMenu = parseFloat(40 / pmWidth) * 100;

        var offsetPer = (((100 - ((pmWidth / winWidth) * 100)))) + "%";

        console.log("Adjust Percentage: " + offsetPer + " | winWidth: " + winWidth + " | pmWidth: " + pmWidth);

        $("div[data-role*='page']").css("margin-left", (pmWidth + "px"));
        $("div[data-role*='page']").css("width", offsetPer);
    };

};

