﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

myapp.Grid.spacerContent_render = function (element, contentItem) {
    // Write code here.
    updateContainersToPush();
};



myapp.Grid.customGrid_render = function (element, contentItem) {
    // Write code here.
    if (document.getElementById('grid')) {
        //$('#grid').w2destroy('grid');
        $('#grid').remove();
    }

    var gridDiv = $("<div id='grid' style='width:1024px; height:768px'></div>");
    //var testDiv = $("<div id='test' style='width:500px; height:40px'>This is a TEST TEST !</div>");

    gridDiv.appendTo($(element));
    //testDiv.appendTo($(element));

    customGrid($(element), $(contentItem));
    $('#grid').w2render('grid');
};


function customGrid(element, contentItem) {
    //var testDiv2 = $("<div id='test2'>" + contentItem.value.FirstName + "</div>");
    //testDiv2.appendTo($(element));

    $().w2grid({
        name: 'grid',
        columns: [
        { field: 'lname', caption: 'Last Name', size: '30%' },
        { field: 'fname', caption: 'First Name', size: '30%' },
        { field: 'email', caption: 'Email', size: '40%' },
        { field: 'sdate', caption: 'Start Date', size: '90px' },
        ],
        records: [
        { recid: 1, fname: 'John', lname: 'doe', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
        { recid: 2, fname: 'Stuart', lname: 'Motzart', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
        { recid: 3, fname: 'Jin', lname: 'Franson', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
        { recid: 4, fname: 'Susan', lname: 'Ottie', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
        { recid: 5, fname: 'Kelly', lname: 'Silver', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
        { recid: 6, fname: 'Francis', lname: 'Gatos', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
        { recid: 7, fname: 'Mark', lname: 'Welldo', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
        { recid: 8, fname: 'Thomas', lname: 'Bahh', email: 'jdoe@gmail.com', sdate: '4/3/2012' }
        ],
    });
};