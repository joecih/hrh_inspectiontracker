﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

myapp.BrowseWorkTypes.spacerContent_render = function (element, contentItem) {
    // Write code here.
    updateContainersToPush();

    // Hide the top header bar
    //$('.msls-header').css('display', 'none');
};

myapp.BrowseWorkTypes.deleteType_execute = function (screen) {
    // Delete Selected
    screen.WorkType.deleteSelected();

    // Save Changes
    myapp.commitChanges().then(function success() {
        //If success
        msls.showMessageBox("Item Deleted!", { title: "Delete" }).then(function () {
            myapp.showBrowseWorkTypes();
        });
        
    }, function fail(e) {
        // If error occurs
        msls.showMessageBox(e.message, { title: e.title }).then(function () {
            // Cancel changes
            myapp.cancelChanges();
        });
    });
};

myapp.BrowseWorkTypes.deleteType_canExecute = function (screen) {
    // Write code here.
    return screen.WorkType.selectedItem !== null;
};


myapp.BrowseWorkTypes.Group1_postRender = function (element, contentItem) {
    // Change push-menu z-index when edit/add popups are used
    $(".multilevelpushmenu_wrapper").css("z-index", "1");
      
};