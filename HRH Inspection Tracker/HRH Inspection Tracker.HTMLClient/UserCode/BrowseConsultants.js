﻿

//myapp.BrowseConsultants.deleteSelected_canExecute = function (screen) {
//    // Write code here.
//    return screen.Items.selectedItem !== null;
//};

//myapp.BrowseConsultants.deleteSelected_execute = function (screen) {
//    // Write code here.
//    screen.Items.deleteSelected();

//    myapp.commitChanges().then(function success() {
//        msls.showMessageBox("Delete is successfull.", { title: "Delete" });
//    }, function fail(e) {
//        // if error occurs
//        msls.showMessageBox(e.message, {title: e.title }).then(function () {
//            // Cancle changes
//            myapp.cancelChanges();
//        });

//    });
    
//};

//myapp.BrowseConsultants.RowTemplate_postRender = function (element, contentItem) {
//    // Write code here.
//    $(element).mouseover(function () {
//        $("ui-li ui-btn ui-btn-up-a ui-btn-up-undefined").css("background-color", "yellow");
//    });

//    $(element).mouseout(function () {
//        $("ui-li ui-btn ui-btn-up-a ui-btn-up-undefined").css("background-color", "lightgray");
//    });
//};
myapp.BrowseConsultants.Consultants_render = function (element, contentItem) {
    // Write code here.
    if ($('#menu')[0].innerHTML == "") {
        // Create Menu
        $('#menu').multilevelpushmenu({
            menu: arrMenu,
            collapsed: true,
            containersToPush: [$("div[data-role*='page']"), $("div[data-role*='header']")],
            overlapWidth: 40,
            onItemClick: function () {
                // First argument is original event object
                var event = arguments[0],
                // Second argument is menu level object containing clicked item (<div> element)
                $menuLevelHolder = arguments[1],
                // Third argument is clicked item (<li> element)
                $item = arguments[2],
                // Fourth argument is instance settings/options object
                options = arguments[3];
                var itemName = $item.find('a:first').text();
                // Collapse menu
                $('#menu').multilevelpushmenu('collapse');
                // Open Page
                if (itemName == 'BrowseConsultants') {
                    myapp.showBrowseConsultants();
                }
                //if (itemName == 'Sub Section') {
                //    myapp.showSubSection();
                //}
                //if (itemName == 'Page Two') {
                //    myapp.showPageTwo();
                //}
            }
        });
    } else {
        // Menu already created
        // Update Menu containers to push
        updateContainersToPush();
    };
};

