/**
 * jquery.multilevelpushmenu.utility.js v2.1.4
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2013-2014, Make IT d.o.o.
 * http://multi-level-push-menu.make.rs
 * https://github.com/adgsm/multi-level-push-menu
 */
                                               
function updateContainersToPush() {

    // Array hold the ContainersToPush
    var arrContainersToPush = [];

    // Loop thru each LightSwitch page named 'page'
    $("div[data-role*='page']").each(function () {
        // Add the page to the array
        var LightSwitchPage = $(this)[0];
        arrContainersToPush.push(LightSwitchPage);
    });

    // Loop thru each LightSwitch header page named 'header'
    //$("div[data-role*='header']").each(function () {
    //    // Add the page to the array
    //    var LightSwitchHeaderPage = $(this)[0];
    //    arrContainersToPush.push(LightSwitchHeaderPage);
    //});

    //// Loop thru each LightSwitch content page
    //$("div[data-role*='content']").each(function () {
    //    // Add the page to the array
    //    var LightSwitchContentPage = $(this)[0];
    //    arrContainersToPush.push(LightSwitchContentPage);
    //});

    // Set the containersToPush for the menu
    $('#menu').multilevelpushmenu('option', 'containersToPush', arrContainersToPush);

};