﻿
var arrMenu = [
    {
        title: 'Main Menu',
        id: 'menuID',
        name: 'MainMenu',
        icon: 'fa fa-reorder',
        items: [
            {
                name: 'Administration',
                icon: 'fa fa-legal',
                link: '#',
                items: [
                    {
                        title: 'Admin Settings',
                        icon: 'fa fa-gears',
                        items: [
                        {
                            name: 'WorkTypes',
                            icon: 'fa fa-folder-o',
                            link: '#'
                        },
                        {
                            name: 'Consultants',
                            icon: 'fa fa-user',
                            link: '#'
                        }
                        ]
                    }
                ]
            },
            {
                name: 'ShowGrid',
                icon: 'fa fa-home',
                link: '#'
            },
            {
                name: 'Inspections',
                icon: 'fa fa-home',
                link: '#'
            },


        ]
    }
];