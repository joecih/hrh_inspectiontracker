﻿/// <reference path="../Scripts/msls-1.0.0.js" />

window.myapp = msls.application;

(function (lightSwitchApplication) {

    var $Entity = msls.Entity,
        $DataService = msls.DataService,
        $DataWorkspace = msls.DataWorkspace,
        $defineEntity = msls._defineEntity,
        $defineDataService = msls._defineDataService,
        $defineDataWorkspace = msls._defineDataWorkspace,
        $DataServiceQuery = msls.DataServiceQuery,
        $toODataString = msls._toODataString;

    function Client(entitySet) {
        /// <summary>
        /// Represents the Client entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this client.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this client.
        /// </field>
        /// <field name="RowVersion" type="Array">
        /// Gets or sets the rowVersion for this client.
        /// </field>
        /// <field name="Name" type="String">
        /// Gets or sets the name for this client.
        /// </field>
        /// <field name="Delivery_Address" type="String">
        /// Gets or sets the delivery_Address for this client.
        /// </field>
        /// <field name="Secondary_Address" type="String">
        /// Gets or sets the secondary_Address for this client.
        /// </field>
        /// <field name="City" type="String">
        /// Gets or sets the city for this client.
        /// </field>
        /// <field name="State" type="String">
        /// Gets or sets the state for this client.
        /// </field>
        /// <field name="ZipCode" type="String">
        /// Gets or sets the zipCode for this client.
        /// </field>
        /// <field name="Phone_Number" type="String">
        /// Gets or sets the phone_Number for this client.
        /// </field>
        /// <field name="Client_Status" type="Boolean">
        /// Gets or sets the client_Status for this client.
        /// </field>
        /// <field name="Contract_Hours" type="Number">
        /// Gets or sets the contract_Hours for this client.
        /// </field>
        /// <field name="Contacts" type="msls.EntityCollection" elementType="msls.application.Contact">
        /// Gets the contacts for this client.
        /// </field>
        /// <field name="Sites" type="msls.EntityCollection" elementType="msls.application.Site">
        /// Gets the sites for this client.
        /// </field>
        /// <field name="InspectionHeaders" type="msls.EntityCollection" elementType="msls.application.InspectionHeader">
        /// Gets the inspectionHeaders for this client.
        /// </field>
        /// <field name="details" type="msls.application.Client.Details">
        /// Gets the details for this client.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function Site(entitySet) {
        /// <summary>
        /// Represents the Site entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this site.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this site.
        /// </field>
        /// <field name="RowVersion" type="Array">
        /// Gets or sets the rowVersion for this site.
        /// </field>
        /// <field name="Site_Name" type="String">
        /// Gets or sets the site_Name for this site.
        /// </field>
        /// <field name="Site_Delivery_Address" type="String">
        /// Gets or sets the site_Delivery_Address for this site.
        /// </field>
        /// <field name="Site_Secondary_Address" type="String">
        /// Gets or sets the site_Secondary_Address for this site.
        /// </field>
        /// <field name="Site_City" type="String">
        /// Gets or sets the site_City for this site.
        /// </field>
        /// <field name="Site_State" type="String">
        /// Gets or sets the site_State for this site.
        /// </field>
        /// <field name="Site_ZipCode" type="String">
        /// Gets or sets the site_ZipCode for this site.
        /// </field>
        /// <field name="Site_Phone" type="String">
        /// Gets or sets the site_Phone for this site.
        /// </field>
        /// <field name="Site_Status" type="Boolean">
        /// Gets or sets the site_Status for this site.
        /// </field>
        /// <field name="Site_Project_Name" type="String">
        /// Gets or sets the site_Project_Name for this site.
        /// </field>
        /// <field name="Client" type="msls.application.Client">
        /// Gets or sets the client for this site.
        /// </field>
        /// <field name="InspectionHeaders" type="msls.EntityCollection" elementType="msls.application.InspectionHeader">
        /// Gets the inspectionHeaders for this site.
        /// </field>
        /// <field name="details" type="msls.application.Site.Details">
        /// Gets the details for this site.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function Contact(entitySet) {
        /// <summary>
        /// Represents the Contact entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this contact.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this contact.
        /// </field>
        /// <field name="RowVersion" type="Array">
        /// Gets or sets the rowVersion for this contact.
        /// </field>
        /// <field name="Company" type="msls.application.Client">
        /// Gets or sets the company for this contact.
        /// </field>
        /// <field name="FirstName" type="String">
        /// Gets or sets the firstName for this contact.
        /// </field>
        /// <field name="LastName" type="String">
        /// Gets or sets the lastName for this contact.
        /// </field>
        /// <field name="Primary_Phone_Number" type="String">
        /// Gets or sets the primary_Phone_Number for this contact.
        /// </field>
        /// <field name="Secondary_Phone_Number" type="String">
        /// Gets or sets the secondary_Phone_Number for this contact.
        /// </field>
        /// <field name="Primary_Email" type="String">
        /// Gets or sets the primary_Email for this contact.
        /// </field>
        /// <field name="Secondary_Email" type="String">
        /// Gets or sets the secondary_Email for this contact.
        /// </field>
        /// <field name="details" type="msls.application.Contact.Details">
        /// Gets the details for this contact.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function Consultant(entitySet) {
        /// <summary>
        /// Represents the Consultant entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this consultant.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this consultant.
        /// </field>
        /// <field name="RowVersion" type="Array">
        /// Gets or sets the rowVersion for this consultant.
        /// </field>
        /// <field name="FirstName" type="String">
        /// Gets or sets the firstName for this consultant.
        /// </field>
        /// <field name="LastName" type="String">
        /// Gets or sets the lastName for this consultant.
        /// </field>
        /// <field name="Primary_Phone_Number" type="String">
        /// Gets or sets the primary_Phone_Number for this consultant.
        /// </field>
        /// <field name="Secondary_Phone_Number" type="String">
        /// Gets or sets the secondary_Phone_Number for this consultant.
        /// </field>
        /// <field name="Primary_Email" type="String">
        /// Gets or sets the primary_Email for this consultant.
        /// </field>
        /// <field name="Secondary_Email" type="String">
        /// Gets or sets the secondary_Email for this consultant.
        /// </field>
        /// <field name="Age" type="Number">
        /// Gets or sets the age for this consultant.
        /// </field>
        /// <field name="InspectionHeaders" type="msls.EntityCollection" elementType="msls.application.InspectionHeader">
        /// Gets the inspectionHeaders for this consultant.
        /// </field>
        /// <field name="details" type="msls.application.Consultant.Details">
        /// Gets the details for this consultant.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function InspectionHeader(entitySet) {
        /// <summary>
        /// Represents the InspectionHeader entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this inspectionHeader.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this inspectionHeader.
        /// </field>
        /// <field name="RowVersion" type="Array">
        /// Gets or sets the rowVersion for this inspectionHeader.
        /// </field>
        /// <field name="InspectionDate" type="Date">
        /// Gets or sets the inspectionDate for this inspectionHeader.
        /// </field>
        /// <field name="Client" type="msls.application.Client">
        /// Gets or sets the client for this inspectionHeader.
        /// </field>
        /// <field name="Site" type="msls.application.Site">
        /// Gets or sets the site for this inspectionHeader.
        /// </field>
        /// <field name="Consultant" type="msls.application.Consultant">
        /// Gets or sets the consultant for this inspectionHeader.
        /// </field>
        /// <field name="InspectionHours" type="Number">
        /// Gets or sets the inspectionHours for this inspectionHeader.
        /// </field>
        /// <field name="Reviewed" type="Boolean">
        /// Gets or sets the reviewed for this inspectionHeader.
        /// </field>
        /// <field name="Processed" type="Boolean">
        /// Gets or sets the processed for this inspectionHeader.
        /// </field>
        /// <field name="InspectionDetail" type="msls.EntityCollection" elementType="msls.application.InspectionDetail">
        /// Gets the inspectionDetail for this inspectionHeader.
        /// </field>
        /// <field name="WorkType" type="msls.application.WorkType">
        /// Gets or sets the workType for this inspectionHeader.
        /// </field>
        /// <field name="details" type="msls.application.InspectionHeader.Details">
        /// Gets the details for this inspectionHeader.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function InspectionDetail(entitySet) {
        /// <summary>
        /// Represents the InspectionDetail entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this inspectionDetail.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this inspectionDetail.
        /// </field>
        /// <field name="RowVersion" type="Array">
        /// Gets or sets the rowVersion for this inspectionDetail.
        /// </field>
        /// <field name="InspectionHeader" type="msls.application.InspectionHeader">
        /// Gets or sets the inspectionHeader for this inspectionDetail.
        /// </field>
        /// <field name="Sec_1_1" type="String">
        /// Gets or sets the sec_1_1 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_1_2" type="String">
        /// Gets or sets the sec_1_2 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_1_3" type="String">
        /// Gets or sets the sec_1_3 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_1_4" type="String">
        /// Gets or sets the sec_1_4 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_1_5" type="String">
        /// Gets or sets the sec_1_5 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_1_6" type="String">
        /// Gets or sets the sec_1_6 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_1_7" type="String">
        /// Gets or sets the sec_1_7 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_2_1" type="String">
        /// Gets or sets the sec_2_1 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_2_2" type="String">
        /// Gets or sets the sec_2_2 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_2_3" type="String">
        /// Gets or sets the sec_2_3 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_2_4" type="String">
        /// Gets or sets the sec_2_4 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_2_5" type="String">
        /// Gets or sets the sec_2_5 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_3_1" type="String">
        /// Gets or sets the sec_3_1 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_3_2" type="String">
        /// Gets or sets the sec_3_2 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_3_3" type="String">
        /// Gets or sets the sec_3_3 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_3_4" type="String">
        /// Gets or sets the sec_3_4 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_3_5" type="String">
        /// Gets or sets the sec_3_5 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_3_6" type="String">
        /// Gets or sets the sec_3_6 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_3_7" type="String">
        /// Gets or sets the sec_3_7 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_3_8" type="String">
        /// Gets or sets the sec_3_8 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_3_9" type="String">
        /// Gets or sets the sec_3_9 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_4_1" type="String">
        /// Gets or sets the sec_4_1 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_4_2" type="String">
        /// Gets or sets the sec_4_2 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_4_3" type="String">
        /// Gets or sets the sec_4_3 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_4_4" type="String">
        /// Gets or sets the sec_4_4 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_4_5" type="String">
        /// Gets or sets the sec_4_5 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_4_6" type="String">
        /// Gets or sets the sec_4_6 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_4_7" type="String">
        /// Gets or sets the sec_4_7 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_5_1" type="String">
        /// Gets or sets the sec_5_1 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_5_2" type="String">
        /// Gets or sets the sec_5_2 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_5_3" type="String">
        /// Gets or sets the sec_5_3 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_5_4" type="String">
        /// Gets or sets the sec_5_4 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_5_5" type="String">
        /// Gets or sets the sec_5_5 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_5_6" type="String">
        /// Gets or sets the sec_5_6 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_6_1" type="String">
        /// Gets or sets the sec_6_1 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_6_2" type="String">
        /// Gets or sets the sec_6_2 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_6_3" type="String">
        /// Gets or sets the sec_6_3 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_6_4" type="String">
        /// Gets or sets the sec_6_4 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_6_5" type="String">
        /// Gets or sets the sec_6_5 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_6_6" type="String">
        /// Gets or sets the sec_6_6 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_7_1" type="String">
        /// Gets or sets the sec_7_1 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_7_2" type="String">
        /// Gets or sets the sec_7_2 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_7_3" type="String">
        /// Gets or sets the sec_7_3 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_7_4" type="String">
        /// Gets or sets the sec_7_4 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_7_5" type="String">
        /// Gets or sets the sec_7_5 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_7_6" type="String">
        /// Gets or sets the sec_7_6 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_7_7" type="String">
        /// Gets or sets the sec_7_7 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_8_1" type="String">
        /// Gets or sets the sec_8_1 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_8_2" type="String">
        /// Gets or sets the sec_8_2 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_8_3" type="String">
        /// Gets or sets the sec_8_3 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_8_4" type="String">
        /// Gets or sets the sec_8_4 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_8_5" type="String">
        /// Gets or sets the sec_8_5 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_8_6" type="String">
        /// Gets or sets the sec_8_6 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_9_1" type="String">
        /// Gets or sets the sec_9_1 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_9_2" type="String">
        /// Gets or sets the sec_9_2 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_9_3" type="String">
        /// Gets or sets the sec_9_3 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_9_4" type="String">
        /// Gets or sets the sec_9_4 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_9_5" type="String">
        /// Gets or sets the sec_9_5 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_9_6" type="String">
        /// Gets or sets the sec_9_6 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_10_1" type="String">
        /// Gets or sets the sec_10_1 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_10_2" type="String">
        /// Gets or sets the sec_10_2 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_10_3" type="String">
        /// Gets or sets the sec_10_3 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_10_4" type="String">
        /// Gets or sets the sec_10_4 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_10_5" type="String">
        /// Gets or sets the sec_10_5 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_10_6" type="String">
        /// Gets or sets the sec_10_6 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_10_7" type="String">
        /// Gets or sets the sec_10_7 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_10_8" type="String">
        /// Gets or sets the sec_10_8 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_10_9" type="String">
        /// Gets or sets the sec_10_9 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_11_1" type="String">
        /// Gets or sets the sec_11_1 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_11_2" type="String">
        /// Gets or sets the sec_11_2 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_11_3" type="String">
        /// Gets or sets the sec_11_3 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_11_4" type="String">
        /// Gets or sets the sec_11_4 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_11_5" type="String">
        /// Gets or sets the sec_11_5 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_11_6" type="String">
        /// Gets or sets the sec_11_6 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_12_1" type="String">
        /// Gets or sets the sec_12_1 for this inspectionDetail.
        /// </field>
        /// <field name="Sec_12_2" type="String">
        /// Gets or sets the sec_12_2 for this inspectionDetail.
        /// </field>
        /// <field name="Comments" type="String">
        /// Gets or sets the comments for this inspectionDetail.
        /// </field>
        /// <field name="details" type="msls.application.InspectionDetail.Details">
        /// Gets the details for this inspectionDetail.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function WorkType(entitySet) {
        /// <summary>
        /// Represents the WorkType entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this workType.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this workType.
        /// </field>
        /// <field name="RowVersion" type="Array">
        /// Gets or sets the rowVersion for this workType.
        /// </field>
        /// <field name="WorkTypes" type="String">
        /// Gets or sets the workTypes for this workType.
        /// </field>
        /// <field name="InspectionHeaders" type="msls.EntityCollection" elementType="msls.application.InspectionHeader">
        /// Gets the inspectionHeaders for this workType.
        /// </field>
        /// <field name="details" type="msls.application.WorkType.Details">
        /// Gets the details for this workType.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function ApplicationData(dataWorkspace) {
        /// <summary>
        /// Represents the ApplicationData data service.
        /// </summary>
        /// <param name="dataWorkspace" type="msls.DataWorkspace">
        /// The data workspace that created this data service.
        /// </param>
        /// <field name="Clients" type="msls.EntitySet">
        /// Gets the Clients entity set.
        /// </field>
        /// <field name="Sites" type="msls.EntitySet">
        /// Gets the Sites entity set.
        /// </field>
        /// <field name="Contacts" type="msls.EntitySet">
        /// Gets the Contacts entity set.
        /// </field>
        /// <field name="Consultants" type="msls.EntitySet">
        /// Gets the Consultants entity set.
        /// </field>
        /// <field name="InspectionHeaders" type="msls.EntitySet">
        /// Gets the InspectionHeaders entity set.
        /// </field>
        /// <field name="InspectionDetails" type="msls.EntitySet">
        /// Gets the InspectionDetails entity set.
        /// </field>
        /// <field name="WorkType" type="msls.EntitySet">
        /// Gets the WorkType entity set.
        /// </field>
        /// <field name="details" type="msls.application.ApplicationData.Details">
        /// Gets the details for this data service.
        /// </field>
        $DataService.call(this, dataWorkspace);
    };
    function DataWorkspace() {
        /// <summary>
        /// Represents the data workspace.
        /// </summary>
        /// <field name="ApplicationData" type="msls.application.ApplicationData">
        /// Gets the ApplicationData data service.
        /// </field>
        /// <field name="details" type="msls.application.DataWorkspace.Details">
        /// Gets the details for this data workspace.
        /// </field>
        $DataWorkspace.call(this);
    };

    msls._addToNamespace("msls.application", {

        Client: $defineEntity(Client, [
            { name: "Id", type: Number },
            { name: "RowVersion", type: Array },
            { name: "Name", type: String },
            { name: "Delivery_Address", type: String },
            { name: "Secondary_Address", type: String },
            { name: "City", type: String },
            { name: "State", type: String },
            { name: "ZipCode", type: String },
            { name: "Phone_Number", type: String },
            { name: "Client_Status", type: Boolean },
            { name: "Contract_Hours", type: Number },
            { name: "Contacts", kind: "collection", elementType: Contact },
            { name: "Sites", kind: "collection", elementType: Site },
            { name: "InspectionHeaders", kind: "collection", elementType: InspectionHeader }
        ]),

        Site: $defineEntity(Site, [
            { name: "Id", type: Number },
            { name: "RowVersion", type: Array },
            { name: "Site_Name", type: String },
            { name: "Site_Delivery_Address", type: String },
            { name: "Site_Secondary_Address", type: String },
            { name: "Site_City", type: String },
            { name: "Site_State", type: String },
            { name: "Site_ZipCode", type: String },
            { name: "Site_Phone", type: String },
            { name: "Site_Status", type: Boolean },
            { name: "Site_Project_Name", type: String },
            { name: "Client", kind: "reference", type: Client },
            { name: "InspectionHeaders", kind: "collection", elementType: InspectionHeader }
        ]),

        Contact: $defineEntity(Contact, [
            { name: "Id", type: Number },
            { name: "RowVersion", type: Array },
            { name: "Company", kind: "reference", type: Client },
            { name: "FirstName", type: String },
            { name: "LastName", type: String },
            { name: "Primary_Phone_Number", type: String },
            { name: "Secondary_Phone_Number", type: String },
            { name: "Primary_Email", type: String },
            { name: "Secondary_Email", type: String }
        ]),

        Consultant: $defineEntity(Consultant, [
            { name: "Id", type: Number },
            { name: "RowVersion", type: Array },
            { name: "FirstName", type: String },
            { name: "LastName", type: String },
            { name: "Primary_Phone_Number", type: String },
            { name: "Secondary_Phone_Number", type: String },
            { name: "Primary_Email", type: String },
            { name: "Secondary_Email", type: String },
            { name: "Age", type: Number },
            { name: "InspectionHeaders", kind: "collection", elementType: InspectionHeader }
        ]),

        InspectionHeader: $defineEntity(InspectionHeader, [
            { name: "Id", type: Number },
            { name: "RowVersion", type: Array },
            { name: "InspectionDate", type: Date },
            { name: "Client", kind: "reference", type: Client },
            { name: "Site", kind: "reference", type: Site },
            { name: "Consultant", kind: "reference", type: Consultant },
            { name: "InspectionHours", type: Number },
            { name: "Reviewed", type: Boolean },
            { name: "Processed", type: Boolean },
            { name: "InspectionDetail", kind: "collection", elementType: InspectionDetail },
            { name: "WorkType", kind: "reference", type: WorkType }
        ]),

        InspectionDetail: $defineEntity(InspectionDetail, [
            { name: "Id", type: Number },
            { name: "RowVersion", type: Array },
            { name: "InspectionHeader", kind: "reference", type: InspectionHeader },
            { name: "Sec_1_1", type: String },
            { name: "Sec_1_2", type: String },
            { name: "Sec_1_3", type: String },
            { name: "Sec_1_4", type: String },
            { name: "Sec_1_5", type: String },
            { name: "Sec_1_6", type: String },
            { name: "Sec_1_7", type: String },
            { name: "Sec_2_1", type: String },
            { name: "Sec_2_2", type: String },
            { name: "Sec_2_3", type: String },
            { name: "Sec_2_4", type: String },
            { name: "Sec_2_5", type: String },
            { name: "Sec_3_1", type: String },
            { name: "Sec_3_2", type: String },
            { name: "Sec_3_3", type: String },
            { name: "Sec_3_4", type: String },
            { name: "Sec_3_5", type: String },
            { name: "Sec_3_6", type: String },
            { name: "Sec_3_7", type: String },
            { name: "Sec_3_8", type: String },
            { name: "Sec_3_9", type: String },
            { name: "Sec_4_1", type: String },
            { name: "Sec_4_2", type: String },
            { name: "Sec_4_3", type: String },
            { name: "Sec_4_4", type: String },
            { name: "Sec_4_5", type: String },
            { name: "Sec_4_6", type: String },
            { name: "Sec_4_7", type: String },
            { name: "Sec_5_1", type: String },
            { name: "Sec_5_2", type: String },
            { name: "Sec_5_3", type: String },
            { name: "Sec_5_4", type: String },
            { name: "Sec_5_5", type: String },
            { name: "Sec_5_6", type: String },
            { name: "Sec_6_1", type: String },
            { name: "Sec_6_2", type: String },
            { name: "Sec_6_3", type: String },
            { name: "Sec_6_4", type: String },
            { name: "Sec_6_5", type: String },
            { name: "Sec_6_6", type: String },
            { name: "Sec_7_1", type: String },
            { name: "Sec_7_2", type: String },
            { name: "Sec_7_3", type: String },
            { name: "Sec_7_4", type: String },
            { name: "Sec_7_5", type: String },
            { name: "Sec_7_6", type: String },
            { name: "Sec_7_7", type: String },
            { name: "Sec_8_1", type: String },
            { name: "Sec_8_2", type: String },
            { name: "Sec_8_3", type: String },
            { name: "Sec_8_4", type: String },
            { name: "Sec_8_5", type: String },
            { name: "Sec_8_6", type: String },
            { name: "Sec_9_1", type: String },
            { name: "Sec_9_2", type: String },
            { name: "Sec_9_3", type: String },
            { name: "Sec_9_4", type: String },
            { name: "Sec_9_5", type: String },
            { name: "Sec_9_6", type: String },
            { name: "Sec_10_1", type: String },
            { name: "Sec_10_2", type: String },
            { name: "Sec_10_3", type: String },
            { name: "Sec_10_4", type: String },
            { name: "Sec_10_5", type: String },
            { name: "Sec_10_6", type: String },
            { name: "Sec_10_7", type: String },
            { name: "Sec_10_8", type: String },
            { name: "Sec_10_9", type: String },
            { name: "Sec_11_1", type: String },
            { name: "Sec_11_2", type: String },
            { name: "Sec_11_3", type: String },
            { name: "Sec_11_4", type: String },
            { name: "Sec_11_5", type: String },
            { name: "Sec_11_6", type: String },
            { name: "Sec_12_1", type: String },
            { name: "Sec_12_2", type: String },
            { name: "Comments", type: String }
        ]),

        WorkType: $defineEntity(WorkType, [
            { name: "Id", type: Number },
            { name: "RowVersion", type: Array },
            { name: "WorkTypes", type: String },
            { name: "InspectionHeaders", kind: "collection", elementType: InspectionHeader }
        ]),

        ApplicationData: $defineDataService(ApplicationData, lightSwitchApplication.rootUri + "/ApplicationData.svc", [
            { name: "Clients", elementType: Client },
            { name: "Sites", elementType: Site },
            { name: "Contacts", elementType: Contact },
            { name: "Consultants", elementType: Consultant },
            { name: "InspectionHeaders", elementType: InspectionHeader },
            { name: "InspectionDetails", elementType: InspectionDetail },
            { name: "WorkType", elementType: WorkType }
        ], [
            {
                name: "Clients_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Clients },
                        lightSwitchApplication.rootUri + "/ApplicationData.svc" + "/Clients(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "Sites_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Sites },
                        lightSwitchApplication.rootUri + "/ApplicationData.svc" + "/Sites(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "Contacts_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Contacts },
                        lightSwitchApplication.rootUri + "/ApplicationData.svc" + "/Contacts(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "Consultants_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Consultants },
                        lightSwitchApplication.rootUri + "/ApplicationData.svc" + "/Consultants(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "ActiveClients", value: function () {
                    return new $DataServiceQuery({ _entitySet: this.Clients },
                        lightSwitchApplication.rootUri + "/ApplicationData.svc" + "/ActiveClients()",
                        {
                        });
                }
            },
            {
                name: "ActiveCustomerSites", value: function (ClientID) {
                    return new $DataServiceQuery({ _entitySet: this.Sites },
                        lightSwitchApplication.rootUri + "/ApplicationData.svc" + "/ActiveCustomerSites()",
                        {
                            ClientID: $toODataString(ClientID, "Int32?")
                        });
                }
            },
            {
                name: "InspectionHeaders_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.InspectionHeaders },
                        lightSwitchApplication.rootUri + "/ApplicationData.svc" + "/InspectionHeaders(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "PendingInspections", value: function () {
                    return new $DataServiceQuery({ _entitySet: this.InspectionHeaders },
                        lightSwitchApplication.rootUri + "/ApplicationData.svc" + "/PendingInspections()",
                        {
                        });
                }
            },
            {
                name: "InspectionDetails_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.InspectionDetails },
                        lightSwitchApplication.rootUri + "/ApplicationData.svc" + "/InspectionDetails(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "WorkType_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.WorkType },
                        lightSwitchApplication.rootUri + "/ApplicationData.svc" + "/WorkType(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "ActiveWorkTypes", value: function () {
                    return new $DataServiceQuery({ _entitySet: this.WorkType },
                        lightSwitchApplication.rootUri + "/ApplicationData.svc" + "/ActiveWorkTypes()",
                        {
                        });
                }
            }
        ]),

        DataWorkspace: $defineDataWorkspace(DataWorkspace, [
            { name: "ApplicationData", type: ApplicationData }
        ])

    });

}(msls.application));
