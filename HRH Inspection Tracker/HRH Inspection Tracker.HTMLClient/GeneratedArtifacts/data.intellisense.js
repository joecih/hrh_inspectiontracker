﻿/// <reference path="data.js" />

(function (lightSwitchApplication) {

    msls._addEntryPoints(lightSwitchApplication.Client, {
        /// <field>
        /// Called when a new client is created.
        /// <br/>created(msls.application.Client entity)
        /// </field>
        created: [lightSwitchApplication.Client]
    });

    msls._addEntryPoints(lightSwitchApplication.Site, {
        /// <field>
        /// Called when a new site is created.
        /// <br/>created(msls.application.Site entity)
        /// </field>
        created: [lightSwitchApplication.Site]
    });

    msls._addEntryPoints(lightSwitchApplication.Contact, {
        /// <field>
        /// Called when a new contact is created.
        /// <br/>created(msls.application.Contact entity)
        /// </field>
        created: [lightSwitchApplication.Contact]
    });

    msls._addEntryPoints(lightSwitchApplication.Consultant, {
        /// <field>
        /// Called when a new consultant is created.
        /// <br/>created(msls.application.Consultant entity)
        /// </field>
        created: [lightSwitchApplication.Consultant]
    });

    msls._addEntryPoints(lightSwitchApplication.InspectionHeader, {
        /// <field>
        /// Called when a new inspectionHeader is created.
        /// <br/>created(msls.application.InspectionHeader entity)
        /// </field>
        created: [lightSwitchApplication.InspectionHeader]
    });

    msls._addEntryPoints(lightSwitchApplication.InspectionDetail, {
        /// <field>
        /// Called when a new inspectionDetail is created.
        /// <br/>created(msls.application.InspectionDetail entity)
        /// </field>
        created: [lightSwitchApplication.InspectionDetail]
    });

    msls._addEntryPoints(lightSwitchApplication.WorkType, {
        /// <field>
        /// Called when a new workType is created.
        /// <br/>created(msls.application.WorkType entity)
        /// </field>
        created: [lightSwitchApplication.WorkType]
    });

}(msls.application));
