﻿/// <reference path="data.js" />

(function (lightSwitchApplication) {

    var $Screen = msls.Screen,
        $defineScreen = msls._defineScreen,
        $DataServiceQuery = msls.DataServiceQuery,
        $toODataString = msls._toODataString,
        $defineShowScreen = msls._defineShowScreen;

    function Main(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the Main screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="Consultants" type="msls.VisualCollection" elementType="msls.application.Consultant">
        /// Gets the consultants for this screen.
        /// </field>
        /// <field name="InspectionHeaders" type="msls.VisualCollection" elementType="msls.application.InspectionHeader">
        /// Gets the inspectionHeaders for this screen.
        /// </field>
        /// <field name="WorkType" type="msls.VisualCollection" elementType="msls.application.WorkType">
        /// Gets the workType for this screen.
        /// </field>
        /// <field name="Sites" type="msls.VisualCollection" elementType="msls.application.Site">
        /// Gets the sites for this screen.
        /// </field>
        /// <field name="details" type="msls.application.Main.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "Main", parameters);
    }

    function AddEditWorkTypes(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the AddEditWorkTypes screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="WorkType" type="msls.application.WorkType">
        /// Gets or sets the workType for this screen.
        /// </field>
        /// <field name="details" type="msls.application.AddEditWorkTypes.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "AddEditWorkTypes", parameters);
    }

    function BrowseWorkTypes(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the BrowseWorkTypes screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="ActiveWorkTypes" type="msls.VisualCollection" elementType="msls.application.WorkType">
        /// Gets the activeWorkTypes for this screen.
        /// </field>
        /// <field name="WorkType" type="msls.VisualCollection" elementType="msls.application.WorkType">
        /// Gets the workType for this screen.
        /// </field>
        /// <field name="details" type="msls.application.BrowseWorkTypes.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "BrowseWorkTypes", parameters);
    }

    function Grid(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the Grid screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="Consultants" type="msls.VisualCollection" elementType="msls.application.Consultant">
        /// Gets the consultants for this screen.
        /// </field>
        /// <field name="details" type="msls.application.Grid.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "Grid", parameters);
    }

    msls._addToNamespace("msls.application", {

        Main: $defineScreen(Main, [
            {
                name: "Consultants", kind: "collection", elementType: lightSwitchApplication.Consultant,
                createQuery: function () {
                    return this.dataWorkspace.ApplicationData.Consultants;
                }
            },
            {
                name: "InspectionHeaders", kind: "collection", elementType: lightSwitchApplication.InspectionHeader,
                createQuery: function () {
                    return this.dataWorkspace.ApplicationData.InspectionHeaders;
                }
            },
            {
                name: "WorkType", kind: "collection", elementType: lightSwitchApplication.WorkType,
                createQuery: function () {
                    return this.dataWorkspace.ApplicationData.WorkType;
                }
            },
            {
                name: "Sites", kind: "collection", elementType: lightSwitchApplication.Site,
                createQuery: function () {
                    return this.dataWorkspace.ApplicationData.Sites;
                }
            }
        ], [
        ]),

        AddEditWorkTypes: $defineScreen(AddEditWorkTypes, [
            { name: "WorkType", kind: "local", type: lightSwitchApplication.WorkType }
        ], [
        ]),

        BrowseWorkTypes: $defineScreen(BrowseWorkTypes, [
            {
                name: "ActiveWorkTypes", kind: "collection", elementType: lightSwitchApplication.WorkType,
                createQuery: function () {
                    return this.dataWorkspace.ApplicationData.ActiveWorkTypes();
                }
            },
            {
                name: "WorkType", kind: "collection", elementType: lightSwitchApplication.WorkType,
                createQuery: function () {
                    return this.dataWorkspace.ApplicationData.WorkType;
                }
            }
        ], [
            { name: "deleteType" }
        ]),

        Grid: $defineScreen(Grid, [
            {
                name: "Consultants", kind: "collection", elementType: lightSwitchApplication.Consultant,
                createQuery: function () {
                    return this.dataWorkspace.ApplicationData.Consultants;
                }
            }
        ], [
        ]),

        showMain: $defineShowScreen(function showMain(options) {
            /// <summary>
            /// Asynchronously navigates forward to the Main screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 0);
            return lightSwitchApplication.showScreen("Main", parameters, options);
        }),

        showAddEditWorkTypes: $defineShowScreen(function showAddEditWorkTypes(WorkType, options) {
            /// <summary>
            /// Asynchronously navigates forward to the AddEditWorkTypes screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 1);
            return lightSwitchApplication.showScreen("AddEditWorkTypes", parameters, options);
        }),

        showBrowseWorkTypes: $defineShowScreen(function showBrowseWorkTypes(options) {
            /// <summary>
            /// Asynchronously navigates forward to the BrowseWorkTypes screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 0);
            return lightSwitchApplication.showScreen("BrowseWorkTypes", parameters, options);
        }),

        showGrid: $defineShowScreen(function showGrid(options) {
            /// <summary>
            /// Asynchronously navigates forward to the Grid screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 0);
            return lightSwitchApplication.showScreen("Grid", parameters, options);
        })

    });

}(msls.application));
