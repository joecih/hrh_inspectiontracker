﻿/// <reference path="viewModel.js" />

(function (lightSwitchApplication) {

    var $parameters = [document.createElement("div"), msls.ContentItem];

    msls._addEntryPoints(lightSwitchApplication.Main, {
        /// <field>
        /// Called when a new Main screen is created.
        /// <br/>created(msls.application.Main screen)
        /// </field>
        created: [lightSwitchApplication.Main],
        /// <field>
        /// Called before changes on an active Main screen are applied.
        /// <br/>beforeApplyChanges(msls.application.Main screen)
        /// </field>
        beforeApplyChanges: [lightSwitchApplication.Main],
        /// <field>
        /// Called after the Group content item has been rendered.
        /// <br/>postRender(HTMLElement element, msls.ContentItem contentItem)
        /// </field>
        Group_postRender: $parameters,
        /// <field>
        /// Called to render the spacerContent content item.
        /// <br/>render(HTMLElement element, msls.ContentItem contentItem)
        /// </field>
        spacerContent_render: $parameters
    });

    msls._addEntryPoints(lightSwitchApplication.AddEditWorkTypes, {
        /// <field>
        /// Called when a new AddEditWorkTypes screen is created.
        /// <br/>created(msls.application.AddEditWorkTypes screen)
        /// </field>
        created: [lightSwitchApplication.AddEditWorkTypes],
        /// <field>
        /// Called before changes on an active AddEditWorkTypes screen are applied.
        /// <br/>beforeApplyChanges(msls.application.AddEditWorkTypes screen)
        /// </field>
        beforeApplyChanges: [lightSwitchApplication.AddEditWorkTypes],
        /// <field>
        /// Called after the Details content item has been rendered.
        /// <br/>postRender(HTMLElement element, msls.ContentItem contentItem)
        /// </field>
        Details_postRender: $parameters,
        /// <field>
        /// Called after the WorkTypes content item has been rendered.
        /// <br/>postRender(HTMLElement element, msls.ContentItem contentItem)
        /// </field>
        WorkTypes_postRender: $parameters
    });

    msls._addEntryPoints(lightSwitchApplication.BrowseWorkTypes, {
        /// <field>
        /// Called when a new BrowseWorkTypes screen is created.
        /// <br/>created(msls.application.BrowseWorkTypes screen)
        /// </field>
        created: [lightSwitchApplication.BrowseWorkTypes],
        /// <field>
        /// Called before changes on an active BrowseWorkTypes screen are applied.
        /// <br/>beforeApplyChanges(msls.application.BrowseWorkTypes screen)
        /// </field>
        beforeApplyChanges: [lightSwitchApplication.BrowseWorkTypes],
        /// <field>
        /// Called to determine if the deleteType method can be executed.
        /// <br/>canExecute(msls.application.BrowseWorkTypes screen)
        /// </field>
        deleteType_canExecute: [lightSwitchApplication.BrowseWorkTypes],
        /// <field>
        /// Called to execute the deleteType method.
        /// <br/>execute(msls.application.BrowseWorkTypes screen)
        /// </field>
        deleteType_execute: [lightSwitchApplication.BrowseWorkTypes],
        /// <field>
        /// Called after the WorkTypeList content item has been rendered.
        /// <br/>postRender(HTMLElement element, msls.ContentItem contentItem)
        /// </field>
        WorkTypeList_postRender: $parameters,
        /// <field>
        /// Called after the Group content item has been rendered.
        /// <br/>postRender(HTMLElement element, msls.ContentItem contentItem)
        /// </field>
        Group_postRender: $parameters,
        /// <field>
        /// Called to render the spacerContent content item.
        /// <br/>render(HTMLElement element, msls.ContentItem contentItem)
        /// </field>
        spacerContent_render: $parameters,
        /// <field>
        /// Called after the Group1 content item has been rendered.
        /// <br/>postRender(HTMLElement element, msls.ContentItem contentItem)
        /// </field>
        Group1_postRender: $parameters,
        /// <field>
        /// Called after the AddWorkType content item has been rendered.
        /// <br/>postRender(HTMLElement element, msls.ContentItem contentItem)
        /// </field>
        AddWorkType_postRender: $parameters,
        /// <field>
        /// Called after the EditWorkType content item has been rendered.
        /// <br/>postRender(HTMLElement element, msls.ContentItem contentItem)
        /// </field>
        EditWorkType_postRender: $parameters,
        /// <field>
        /// Called after the deleteType content item has been rendered.
        /// <br/>postRender(HTMLElement element, msls.ContentItem contentItem)
        /// </field>
        deleteType_postRender: $parameters,
        /// <field>
        /// Called after the WorkType content item has been rendered.
        /// <br/>postRender(HTMLElement element, msls.ContentItem contentItem)
        /// </field>
        WorkType_postRender: $parameters,
        /// <field>
        /// Called after the WorkTypeTemplate content item has been rendered.
        /// <br/>postRender(HTMLElement element, msls.ContentItem contentItem)
        /// </field>
        WorkTypeTemplate_postRender: $parameters
    });

    msls._addEntryPoints(lightSwitchApplication.Grid, {
        /// <field>
        /// Called when a new Grid screen is created.
        /// <br/>created(msls.application.Grid screen)
        /// </field>
        created: [lightSwitchApplication.Grid],
        /// <field>
        /// Called before changes on an active Grid screen are applied.
        /// <br/>beforeApplyChanges(msls.application.Grid screen)
        /// </field>
        beforeApplyChanges: [lightSwitchApplication.Grid],
        /// <field>
        /// Called after the Group content item has been rendered.
        /// <br/>postRender(HTMLElement element, msls.ContentItem contentItem)
        /// </field>
        Group_postRender: $parameters,
        /// <field>
        /// Called to render the spacerContent content item.
        /// <br/>render(HTMLElement element, msls.ContentItem contentItem)
        /// </field>
        spacerContent_render: $parameters,
        /// <field>
        /// Called to render the customGrid content item.
        /// <br/>render(HTMLElement element, msls.ContentItem contentItem)
        /// </field>
        customGrid_render: $parameters
    });

}(msls.application));
